﻿using BG.Core.IRepository;
using BG.Core.IService;
using BG.Core.Model;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BG.Sql.Service
{
    public class HostedService : BackgroundService
    {
        private readonly IEmailService _emailService;
        private readonly ILogger<HostedService> _hostedServiceLogging;
        private readonly IEmailTaskRepository _emailTaskRepository;

        public HostedService(
            IEmailService emailService,
            ILogger<HostedService> hostedServiceLogging,
            IEmailTaskRepository emailTaskRepository)
        {
            _emailService = emailService;
            _hostedServiceLogging = hostedServiceLogging;
            _emailTaskRepository = emailTaskRepository;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _hostedServiceLogging.LogDebug("BG Hosted Service is starting");
            stoppingToken.Register(() => _hostedServiceLogging.LogDebug("BG Hosted is stopping."));
            while (!stoppingToken.IsCancellationRequested)
            {
                _hostedServiceLogging.LogDebug("BG Hosted is running in background");

                var pendingEmailTasks = await _emailTaskRepository.GetEmailTaskBySearchCriteria(new SearchCriteria { IsEmailSent = false });
                var toAddress = pendingEmailTasks.Where(x => x.ToAddress)
                    .Select(x => new EmailAddress { Address = x.Address, Name = x.Name })
                    .ToList();
                var fromAddress = pendingEmailTasks.Where(x => !x.ToAddress)
                    .Select(x => new EmailAddress { Address = x.Address, Name = x.Name })
                    .ToList();
                var content = "BG Demo Application is sending message to you";
                var subject = "BG Demo Application";


                var pendingEmailMessage = new EmailMessage
                {
                    FromAddresses = fromAddress,
                    ToAddresses = toAddress,
                    Content = content,
                    Subject = subject
                };

                _emailService.SendEmail(pendingEmailMessage);
                await Task.Delay(1000 * 60 * 5, stoppingToken);
               
            }
            _hostedServiceLogging.LogDebug("Demo service is stopping");
        }
    }
}
