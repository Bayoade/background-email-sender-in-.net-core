﻿using AutoMapper;
using BG.Core.Helpers;
using BG.Core.IRepository;
using BG.Core.Model;
using BG.Sql.Context;
using BG.Sql.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BG.Sql.Repository
{
    public class EmailTaskRepository : IEmailTaskRepository
    {
        private readonly BGDemoContext _dbContext;

        public EmailTaskRepository(BGDemoContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IList<EmailTaskProfile>> GetEmailTasks()
        {
            var emailTasks = await _dbContext.EmailTasks.ToListAsync();
            return Mapper.Map<List<EmailTaskProfile>>(emailTasks);
        }


        public async Task<IList<EmailTaskProfile>> GetEmailTaskBySearchCriteria(SearchCriteria searchCriteria)
        {
            var query = _dbContext.EmailTasks.AsQueryable();
            query = query.Where(x => x.IsEmailSent == searchCriteria.IsEmailSent);

            if (!searchCriteria.Name.IsNull())
            {
                query = query.Where(x => searchCriteria.Name.ToLower()
                .Contains(x.Name.ToLower()));

            }

            var emailTasks = await query.ToListAsync();

            return Mapper.Map<List<EmailTaskProfile>>(emailTasks);
        }

        public Task UpdateEmailTasks(List<Guid> ids)
        {
            var emailTaskProfiles = new List<EmailTask>();
            foreach (var id in ids)
            {
                emailTaskProfiles.Add(new EmailTask { Id = id, IsEmailSent = true });
            }

            _dbContext.EmailTasks.AttachRange(emailTaskProfiles);

            _dbContext.EmailTasks.UpdateRange(emailTaskProfiles);

            return _dbContext.SaveChangesAsync();
        }
    }
}
