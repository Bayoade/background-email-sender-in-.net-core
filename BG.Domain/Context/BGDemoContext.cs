﻿using BG.Sql.Model;
using Microsoft.EntityFrameworkCore;

namespace BG.Sql.Context
{
    public class BGDemoContext : DbContext
    {
        public BGDemoContext(DbContextOptions<BGDemoContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");

            var informationModelBuilder = modelBuilder.Entity<EmailTask>().ToTable("EmailTasks");

            informationModelBuilder.HasIndex(x => x.Id);
        }

        internal DbSet<EmailTask> EmailTasks { get; set; }
    }
}
