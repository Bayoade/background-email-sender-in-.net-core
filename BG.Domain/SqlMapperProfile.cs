﻿using AutoMapper;
using BG.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace BG.Sql
{
    public class SqlMapperProfile : Profile
    {
        public SqlMapperProfile()
        {
            CreateMap<EmailAddress, EmailTaskProfile>()
                .ForMember(x => x.IsEmailSent, y => y.Ignore())
                .ForMember(x => x.Id, y => y.Ignore())
                .ForMember(x => x.ToAddress, y => y.Ignore())
                .ReverseMap();
        }
    }
}
