﻿using System;

namespace BG.Core.Model
{
    public class EmailTaskProfile
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public bool IsEmailSent { get; set; }

        public bool ToAddress { get; set; }
    }
}
