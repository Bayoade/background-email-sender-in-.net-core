﻿namespace BG.Core.Model
{
    public class SearchCriteria
    {
        public string Name { get; set; }

        public bool IsEmailSent { get; set; }
    }
}
