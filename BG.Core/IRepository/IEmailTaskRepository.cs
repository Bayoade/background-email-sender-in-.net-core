﻿using BG.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BG.Core.IRepository
{
    public interface IEmailTaskRepository
    {
        Task<IList<EmailTaskProfile>> GetEmailTasks();

        Task<IList<EmailTaskProfile>> GetEmailTaskBySearchCriteria(SearchCriteria searchCriteria);

        Task UpdateEmailTasks(List<Guid> taskProfiles);
    }
}
