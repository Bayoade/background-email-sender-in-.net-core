﻿using System.Collections.Generic;
using System.Linq;

namespace BG.Core.Helpers
{
    public static class ObjectExtensionMethods
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || source.Count() == 0;
        }

        public static bool IsNull(this object source)
        {
            return source == null;
        }
    }
}
