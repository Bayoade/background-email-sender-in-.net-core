﻿using System.Threading;
using System.Threading.Tasks;

namespace BG.Core.IService
{
    public interface IHostedService
    {
        Task StartAsyn(CancellationToken cancellationToken);

        Task StopAsync(CancellationToken cancellationToken);
    }
}
