﻿using BG.Core.Model;
using System.Collections.Generic;

namespace BG.Core.IService
{
    public interface IEmailService
    {
        void SendEmail(EmailMessage emailMessage);

        IList<EmailMessage> ReceiveEmail(int maxCount = 10);
    }
}
