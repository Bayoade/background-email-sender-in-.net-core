﻿
using BG.Core.IRepository;
using BG.Core.IService;
using BG.Core.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Threading.Tasks;
using System.Linq;
using BG.Core.Helpers;
using System.Collections.Generic;

namespace BackgroundProcesses.AppServices
{
    public class ScheduledJob : IJob
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ScheduledJob> _scheduleJobLogging;
        private readonly IEmailService _emailService;
        private readonly IEmailTaskRepository _emailTaskRepository;


        public ScheduledJob(IConfiguration configuration,
            IEmailService emailService,
            ILogger<ScheduledJob> scheduleJobLogging,
            IEmailTaskRepository emailTaskRepository)
        {
            _scheduleJobLogging = scheduleJobLogging;
            _configuration = configuration;
            _emailService = emailService;
            _emailTaskRepository = emailTaskRepository;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var now = DateTime.Now;
            var stoppingToken = context.CancellationToken;
            
            while (now.Hour == 15)
            {
                _scheduleJobLogging.LogDebug("BG Schedule Job Service is running");
                stoppingToken.Register(() => _scheduleJobLogging.LogDebug("BG Hosted is stopping."));
                while (!stoppingToken.IsCancellationRequested)
                {
                    _scheduleJobLogging.LogDebug("BG Schedule is running in background");

                    var pendingEmailTasks = await _emailTaskRepository.GetEmailTaskBySearchCriteria(new SearchCriteria { IsEmailSent = false });
                    if (!pendingEmailTasks.IsNullOrEmpty())
                    {
                        var toAddress = pendingEmailTasks.Where(x => x.ToAddress)
                        .Select(x => new EmailAddress { Address = x.Address, Name = x.Name })
                        .ToList();
                        var fromAddress = pendingEmailTasks.Where(x => !x.ToAddress)
                            .Select(x => new EmailAddress { Address = x.Address, Name = x.Name })
                            .ToList();
                        var subject = "BG Demo Application";
                        var content = "This is a newsletter from BG Background Task";
                        var pendingEmailMessage = new EmailMessage
                        {
                            FromAddresses = fromAddress,
                            ToAddresses = toAddress,
                            Content = content,
                            Subject = subject
                        };

                        _emailService.SendEmail(pendingEmailMessage);
                        await _emailTaskRepository.UpdateEmailTasks(pendingEmailTasks.Select(x => x.Id).ToList());
                    }

                    await Task.Delay(1000 * 60 * 5, stoppingToken);
                }

                _scheduleJobLogging.LogDebug("Demo service is stopping");

                await Task.CompletedTask;
            }
        }
    }
}