﻿using BG.Core.Model;
using BG.Sql.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BackgroundProcesses.Bootstrap
{
    public static class ConfigureEmailSettings
    {
        public static void AddEmailConfiguration(this IServiceCollection services)
        {
            services.AddSingleton<IEmailConfiguration>(IocContainer.Configuration
                .GetSection("EmailConfiguration").Get<EmailConfiguration>());
        }
    }
}
