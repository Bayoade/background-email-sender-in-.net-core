﻿using BG.Sql.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BackgroundProcesses.Bootstrap
{
    public static class ConfigureDbContext
    {
        public static void AddBGDbContext(this IServiceCollection services)
        {
            services.AddDbContext<BGDemoContext>(options =>
            {
                options.UseSqlServer(IocContainer.Configuration.GetConnectionString("BGDemoConnectionKey"));
            });
        }
    }
}
