﻿using AutoMapper;
using BG.Sql;
using Microsoft.Extensions.DependencyInjection;

namespace BackgroundProcesses.Bootstrap
{
    public static class ConfigureAutoMaper
    {
        public static void AddAutoMapperProfiles(this IServiceCollection services)
        {
            Mapper.Initialize(conf =>
            {
                conf.AddProfile<SqlMapperProfile>();
            });
        }
    }
}
