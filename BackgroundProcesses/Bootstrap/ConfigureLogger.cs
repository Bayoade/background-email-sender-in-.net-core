﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System.IO;

namespace BackgroundProcesses.Bootstrap
{
    public static class ConfigureLogger
    {
        public static void AddLoggerConfiguration(this IApplicationBuilder services, IHostingEnvironment env, ILoggerFactory logger)
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .MinimumLevel.Debug()
                .WriteTo.File(Path.Combine(env.ContentRootPath, "backgroundmailer-log.txt"))
                .CreateLogger();

            logger.AddSerilog();
        }
    }
}
