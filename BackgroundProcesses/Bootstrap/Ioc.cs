﻿using Microsoft.Extensions.Configuration;

namespace BackgroundProcesses.Bootstrap
{
    public static class IocContainer
    {
        public static IConfiguration Configuration { get; set; } 
    }
}
