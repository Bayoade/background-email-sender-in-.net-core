﻿using Autofac;
using BG.Core.IRepository;
using BG.Core.IService;
using BG.Sql.Repository;
using BG.Sql.Service;

namespace BackgroundProcesses.Bootstrap
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<HostedService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<EmailService>()
                .As<IEmailService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<EmailTaskRepository>()
                .As<IEmailTaskRepository>()
                .InstancePerLifetimeScope();
        }
    }
}
